/**
 * This is the imlpementation of the FIND and the random Quick Sort algorithm.
 * The below text is very messy, we have adjusted it to every experiment we
 * wanted to perform.
 *
 * Further for would clearly be to do a cleanup of the code, and parameterize
 * the experiment selection.
 */

#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <fstream>
#include <math.h>

using namespace std;

random_device rd;
ifstream rand_bits;

unsigned int trueRandomness(){
	int ret;
	rand_bits >> ret;
	rand_bits.get();
	while(rand_bits.good()){
		cout << rand_bits.get() << " ";
	}
	return ret;	
}

typedef struct data {
	//for find
	int k;
	int hits;
	
	int n;
	int cmps; //comparisons
	float avgRec; //average recursion depth
	int calls;
	float numRuns;
} data_t;

/**
 * returns a random number in [0 ; max]
 */
int randomGen(int max){
	//following ~27sek
	uniform_int_distribution<int> distribution(0,max-1);//-1 because they are inclusive
	return distribution(rd);
	
	//following solution ~27sek, modulus does not provide uniform distribution
	//int ret = rd() % max;
	//return ret;//DUMMY
}

void printVector(vector<unsigned int> vec){
	for(unsigned int i=0 ; i<vec.size() ; i++){
		cerr << vec[i] << ", ";
	}
	cerr << " .-" << endl;
}
void printVector(vector<unsigned int> *vec){
	for(unsigned int i=0 ; i<vec->size() ; i++){
		cerr << vec->at(i) << ", ";
	}
	cerr << " <-" << endl;
}

/**
 * the find function 
 *
 * keep lists in scope for automatic garbage colletion
 */
unsigned int find(vector<unsigned int> L, unsigned int k, unsigned int* rec, unsigned int* cmps){

	//+1 to recursion depth (++ binds stronger than *)
	(*rec)++;
	//find a pivot element
	unsigned int e = L[randomGen(L.size())];
	
	//allocating on stack for implicit garbage collection
	vector<unsigned int> L1, L2;
	for(unsigned int i=0 ; i<L.size() ; i++){
		(*cmps)++;
		if(L[i] < e){
			L1.push_back(L[i]);
		}
		else if(L[i] > e){
			L2.push_back(L[i]);
		}
		
		//no dublicates, e is implicitly removed
	}
	
	//all the comparing stuff
	if(L1.size() == k)
		return e;
	
	if(L1.size() > k)
		return find(L1, k, rec, cmps);
		
	return find(L2, k - 1 - L1.size(), rec, cmps);
}

/**
 * randomised quik sort
 */
vector<unsigned int>* rand_qs(vector<unsigned int> L, int* recDepth, int* cmprs){
	(*recDepth)++;
	//base case
	if(L.size() < 1){
		vector<unsigned int>* l  = new vector<unsigned int>();
		return l;
	}
	
	//finding pivot
	unsigned int pivot = L[randomGen(L.size())];
	
	//splitting the list
	vector<unsigned int> L1, L2;
	for(int i=0 ; i<L.size() ; i++){
		
		(*cmprs)++;
		if(L[i] < pivot)
			L1.push_back(L[i]);
		if(L[i] > pivot)
			L2.push_back(L[i]);
		//implicitly remove the pivot element from the sublists
	}
	
	//the merging, these are on the heap
	vector<unsigned int> *res = new vector<unsigned int>(),
		*res1 = rand_qs(L1, recDepth, cmprs),
		*res2 = rand_qs(L2, recDepth, cmprs);
	
	for(int i=0 ; i<res1->size() ; i++)
		res->push_back(res1->at(i));
	
	//adding pivot element
	res->push_back(pivot);
	
	for(int i=0 ; i<res2->size() ; i++)
		res->push_back(res2->at(i));
	
	//garbage collection
	delete res1;
	res1 = NULL;
	delete res2;
	res2 = NULL;
	
	return res;
}

//performs a test with size n
data_t runFind(int n, int k){
	vector<unsigned int> numbers;
	for(unsigned int i=0 ; i< n ; i++){
		numbers.push_back(i);
	}
	//shuffle array using our number generator
	random_shuffle(numbers.begin() , numbers.end(), randomGen);
	
	//int numK = 5;
	//int k[10] = {n-1, n/2, n/3, n/4, 1};
	//for(int i=0 ; i<numK ; i++){
	
	unsigned int totalRec = 0, totalCmps=0; 
	int t=0, hits=0;
	for(; t<n*2 ; t++){
		unsigned int rec = 0, cmps = 0;
		unsigned int ret = find(numbers, k, &rec, &cmps);
		//cerr << n << "\t" << ret << "\t" << rec << "\t" << cmps << endl;
		totalRec += rec;
		totalCmps += cmps;
		if(rec > (1 + 4*log(n)))
			hits++;
		
		//if(t % 10000 == 0)
		//	cerr << t << endl;
	}
	data_t ret;
	
	ret.cmps = ((float)totalCmps)/t;
	ret.avgRec = ((float) totalRec)/t;
	ret.numRuns = t;
	ret.n = n;
	ret.k = k;
	ret.hits = hits;
	ret.calls = totalRec;
	return ret;
	
	cout << n << "\t" << t << "\t" << totalRec << "\t";
	cout << ((float) totalRec)/t << "\t" << k << "\t";
	cout << (1+(2*log(n))) << endl;
			
	//}
}

data_t runQS(int n){
	//the list
	vector<unsigned int> numbers;
	for(unsigned int i=0 ; i< n ; i++){
		numbers.push_back(i);
	}
	
	//shuffle array using our number generator
	random_shuffle(numbers.begin() , numbers.end(), randomGen);
	
	unsigned int totalRec = 0, totalCmps=0;
	int t=0;
	for(; t<n*1000000 ; t++){
		int rec = 0, cmps = 0;
		vector<unsigned int>* ret = rand_qs(numbers, &rec, &cmps);
		delete ret;//need for garbage collection
		//cerr << n << "\t" << ret << "\t" << rec << "\t" << cmps << endl;
		totalRec += rec;
		totalCmps += cmps;
		int limit = 6*n*log(n); 
		if(cmps >= limit)
			cout << "HIT: " << t  << " cmps: " << cmps << endl;
		if(t % 10000 == 0){
			cout << "SHUFFLE: " << t << " cmps: " << cmps << " limit: " << limit << endl;
			random_shuffle(numbers.begin() , numbers.end(), randomGen);
		}
	}
	data_t ret;
	ret.n = n;
	ret.numRuns = t;
	ret.cmps = totalCmps;
	return ret;
}

void print_d_find(data_t d){
	cout << d.n << "\t";			//n
	cout << d.numRuns << "\t";		//#runs
	cout << d.avgRec << "\t";		//rec depth
	cout << d.cmps << "\t";	//comparisons
	cout << d.k << "\t";
	cout << (1+(2*log(d.n))) << endl; //bound
	//cout << "\t" << d.hits << "\t" << ((float) d.hits)/d.numRuns<< endl;
}

void print_d_rand_qs(data_t d){
	cout << d.n << "\t" << d.numRuns << "\t";
	cout << d.cmps / d.numRuns << endl;
}

int main(int argc, char* argv[]){	
	//cerr << "n\tresult\t#calls\t#comparisons" << endl;
	//cout << "n\t#runs.\tAvg#cll\tAvgCmps\tK\tuBound" << endl;
	cout << "n\t#runs.\tAvg#cmps" << endl;

	print_d_rand_qs(runQS(10));
	
	
	return 1;
	/*
	for(int n=100 ; n < 10000 ; n = n+100){
		print_d_find(runFind(n, 1));
	}
	for(int n=100 ; n < 10000 ; n = n+100){
		print_d_find(runFind(n, n/2));
	}
	return 1;*/
	
	print_d_find(runFind(100, 0));
	print_d_find(runFind(1000, 0));
	print_d_find(runFind(10000, 0));
	
	print_d_find(runFind(100, 50));
	print_d_find(runFind(1000, 500));
	print_d_find(runFind(10000, 5000));
	return 1;
	
	return 0;
}
